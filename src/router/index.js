import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  { 
    path: '/create',
    name: 'CreateNote',
    component: () => import('../components/CreateNote/CreateNote.vue')
  },
  {
    path: '/card/:id',
    name: 'CardInformation',
    component: () => import('../components/CardInformation/CardInformation.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
