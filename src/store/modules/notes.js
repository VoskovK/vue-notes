export default {
    state: {
        notesList: JSON.parse(localStorage.getItem('notes') || '[]'),
        query: ''
    },

    actions: {
        createNotes({ commit }, note) {
            commit('CREATE_NOTE', note);
        },

        deleteNotes({ commit }, note) {
            commit('DELETE_NOTE', note);
        },

        updateNotes({ commit }, note) {
            commit('UPDATE_NOTE', note);
        },

        searchNotes({ commit }, note) {
            commit('SEARCH_NOTE', note);
        }
    },

    mutations: {
        CREATE_NOTE(state, note) {
            if (note) {
                state.notesList.push(note);
                localStorage.setItem('notes', JSON.stringify(state.notesList));
            }
        },

        DELETE_NOTE(state, note) {
            let idx = state.notesList.findIndex(n => n.id == note.id);

            if (idx) {
                state.notesList.splice(idx, 1);
                localStorage.setItem('notes', JSON.stringify(state.notesList));
            }
        },

        UPDATE_NOTE(state, updateNote) {
            const idx = state.notesList.findIndex(note => note.id === updateNote.id);

            if (idx > -1) {
                state.notesList.splice(idx, 1, updateNote);
                localStorage.setItem('notes', JSON.stringify(state.notesList));
            } else {
                state.notesList.push(updateNote);
            }
        },

        SEARCH_NOTE(state, note) {
            state.query = note;
        }
    },

    getters: {
        getAllNotes(state) {
            return state.notesList;            
        },

        getNotesById: state => id => state.notesList.find(n => n.id === id),

        getCountNotes(state) {
            return state.notesList.length;
        }
    },

    namespaced: true
}